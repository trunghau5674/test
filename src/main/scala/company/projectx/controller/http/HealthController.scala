package company.projectx.controller.http


import com.twitter.finagle.http.Request
import com.twitter.finatra.http.Controller
import com.twitter.finatra.request.{Header, QueryParam, RouteParam}
import javax.inject.Inject
import com.twitter.finatra.http.request.HttpForward
import com.twitter.finatra.http.jsonpatch.{JsonPatch, JsonPatchOperator, JsonPatchUtility}
import scala.util.parsing.json.JSONObject

/**
  * Created by SangDang on 9/18/16.
  */
case class HiRequest(id: Long, name: String,
                    @Header("token") allow: String
                    )
case class FromRouteRequest(
                             @RouteParam("entity") resource: String,
                             @RouteParam id: String,
                             @QueryParam verbose: Boolean = false
                           )

class HealthController @Inject()(forward: HttpForward)(jsonPatchOperator: JsonPatchOperator) extends Controller {
  get("/ping") {
    request: Request => {
      logger.info("ping")
      Map("status" -> "ok", "data" -> "pong", "user" -> "Unknowm")
    }
  }
  get("/bar") { request: Request =>
    forward(request, "/ping")
  }
  post("/hi") { hiRequest: HiRequest =>
    if (hiRequest.allow == "TrungHau") "Hello " + hiRequest.name + " with id " + hiRequest.id
    else "You aren't allow to change anything"
  }
  get("/foo/:entity/:id") { request: FromRouteRequest =>
    if (request.verbose == true) s"The resource is ${request.resource} and the id = ${request.id} too long"
    else s"The resource is ${request.resource} and the id = ${request.id} too short"
  }
}



