package company.projectx.controller.http.filter

import com.fasterxml.jackson.core.JsonParseException
import com.google.inject.Singleton
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder
import com.twitter.finatra.json.internal.caseclass.exceptions.CaseClassMappingException
import com.twitter.inject.Logging
import company.projectx.exception.BaseException
import company.projectx.module.{ApiError, BaseResponse}
import javax.inject.Inject

/**
  * @author anhlt
  */
@Singleton
class CommonExceptionMapping @Inject()(response: ResponseBuilder) extends ExceptionMapper[Throwable] with Logging {

  override def toResponse(request: Request, ex: Throwable): Response = {
    logError(request,ex)
    val error = ex match {
      case ex: BaseException => ApiError(ex.getStatus.code, reason = ex.reason, ex.getMessage)
      case _ => ApiError(Status.InternalServerError.code,BaseException.InternalError, ex.getMessage)
    }
    response.status(error.code).json(
      BaseResponse(success = false, data = None, error = Some(error)
    ))
  }

  private def logError(request: Request, ex: Throwable): Unit = {
    logger.error(s"${request.contentString} => ${ex.getClass.getName}: ${ex.getMessage}",ex)
  }
}

@Singleton
class CaseClassExceptionMapping @Inject()(response: ResponseBuilder) extends ExceptionMapper[CaseClassMappingException] with Logging {
  override def toResponse(request: Request, ex: CaseClassMappingException): Response = {
    logError(request,ex)
    response.badRequest.json(BaseResponse(success = false,
        data = None,
        error = Some(ApiError(Status.BadRequest.code, reason = BaseException.BadRequest, ex.errors.head.getMessage))
    ))
  }
  private def logError(request: Request, ex: Throwable): Unit = {
    logger.error(s"${request.contentString} => ${ex.getClass.getName}: ${ex.getMessage}",ex)
  }
}

@Singleton
class JsonParseExceptionMapping @Inject()(response: ResponseBuilder) extends ExceptionMapper[JsonParseException] with Logging {
  override def toResponse(request: Request, ex: JsonParseException): Response = {
    logError(request,ex)
    response.badRequest.json(BaseResponse(success = false,
        data = None,
        error = Some(ApiError(Status.BadRequest.code,BaseException.BadRequest, ex.getMessage))
    ))
  }
  private def logError(request: Request, ex: Throwable): Unit = {
    logger.error(s"${request.contentString} => ${ex.getClass.getName}: ${ex.getMessage}",ex)
  }
}

